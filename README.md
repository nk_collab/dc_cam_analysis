# An Analysis of Speed Cameras in DC

## Data

1. Locations of Speed Cameras and Red Light Cameras
2. Census Tract Shapefiles from Census Bureau
3. American Community Survey Median Income Data (Census Tract)
4. Race and Ethnicity, Census Bureau, via Data Ferrett (Census Tract)
5. Crime Data, DC Open Data
6. Poverty Data, DC Open Data
7. Moving Violations, DC Open Data
8. Parking Violations, DC Open Data

## Exploratory Analyis

1. Geospatial Map with Locations of Cameras and Median Income of Census Tracts
2. Geospatial Map with Locations of Cameras and Percentage of Non-white, Non-hispanic residents in Census Tract

## Model

- Multiple Regression (regressing number of cameras on composition of minority residents, controlling for crime, income)
- Probit/Logit Model

-----
*Exploratory analysis by Kim Kreiss and Nathan Heinrich*
