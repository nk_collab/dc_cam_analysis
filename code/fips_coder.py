import requests
import csv
import numpy as np
import pandas as pd
import multiprocessing as mp

def get_fips(lon, lat):
    fcc_address = 'https://geo.fcc.gov/api/census/block/find?latitude=%s&longitude=%s&showall=true&format=json' % (lat, lon)
    r = requests.get(fcc_address)

    try:
        id = int(r.json()['Block']['FIPS'])

    except:
        id = np.nan

    return id

def code_row(row):
    fipsdat = {}
    print "Requesting observation #%s." % row['i']
    #Read the data from appropriate variables in raw csv file from FDA.
    fipsdat['fips'] = get_fips(row['Longitude'], row['Latitude'])
    row.update(fipsdat)

    return row

def read_inspections(rawdat):
    n = len(open('../data/input/%s' % rawdat).readlines())
    with open('../data/input/%s' % rawdat, 'rb') as csvfile:
         reader = csv.DictReader(csvfile)
         rows = []
         i = 1
         for row in reader:
             indexer = {}
             indexer['i'] = i
             row.update(indexer)
             rows.append(row)
             i += 1

    print "Total of %s requests." % len(rows)
    return rows

def pool_fips(row_list):

    p = mp.Pool(mp.cpu_count())
    n = len(row_list)
    fips_coded = p.map(func=code_row, iterable=row_list, chunksize=1)

    df = pd.DataFrame(fips_coded)

    return df

def main():
    rows = read_inspections('Chicago_Crimes_2012_to_2017.csv')
    data_with_fips = pool_fips(rows)
    data_with_fips.to_csv('../data/input/fips_coded_crimes.csv', index = False)

if __name__ == "__main__":
    main()
